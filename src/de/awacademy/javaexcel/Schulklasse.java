package de.awacademy.javaexcel;

public class Schulklasse {
    private String kuerzel;

    private String name;

    private int klassenstufe;


    public Schulklasse(String kuerzel, String name) {
        this.kuerzel = kuerzel;
        this.name = name;
        this.klassenstufe = kuerzel.charAt(0) - '0';
    }

    public String getKuerzel() {
        return kuerzel;
    }

    public String getName() {
        return name;
    }

    public int getKlassenstufe() {
        return klassenstufe;
    }

    @Override
    public String toString() {
        return "Schulklasse{" +
                "kuerzel='" + kuerzel + '\'' +
                ", name='" + name + '\'' +
                ", klassenstufe=" + klassenstufe +
                '}';
    }
}
