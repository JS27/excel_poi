package de.awacademy.javaexcel;

import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class A04_SchuleReader_Aufgabe1 {

    private List<Lehrkraft> lehrkraefte = new ArrayList<Lehrkraft>();

    private List<Fach> faecher = new ArrayList<>();

    private List<Schulklasse> schulklassen = new ArrayList<>();


    public static void main(String[] args) throws IOException {
        new A04_SchuleReader_Aufgabe1().run();
    }

    private void run() throws IOException {
        Workbook workbook = WorkbookFactory.create(new File("schule.xlsx"), null, true);

        // Aufgabe 1: Lies die Arbeitsblätter aus der Excel-Datei ein und

        Row row;
        Cell cell;
        List<String> nameLehrkraefte = holtNameLehrkraefte(workbook);
        List<Double> sollSundenzahl = new ArrayList<Double>();
        holtKuerzelKlassenname(workbook);
        faecher = holtListeFaecherAlle(workbook);

        // gib jeden Datensatz mit dessen toString()-Methode auf der Konsole aus.
        faecher.forEach(s -> System.out.println(s.toString()));
        lehrkraefte.forEach(s -> System.out.println(s.toString()));
        schulklassen.forEach(s -> System.out.println(s.toString()));
    }

    public List<String> holtNameLehrkraefte(Workbook workbook) {
        List<String> nameLehrkraefte = new ArrayList<String>();
        Row row;
        Cell cell;
        for (int i = 1; i <= 6; i++) {
            Sheet sheet = workbook.getSheetAt(0);
            row = sheet.getRow(i);
            cell = row.getCell(0);
            nameLehrkraefte.add(cell.toString());
            lehrkraefte.add(new Lehrkraft(cell.toString(), holtListeFaecher(workbook, i), holtSollStundenzahl(row, cell, sheet, i)));
        }
        return nameLehrkraefte;
    }


    public int holtSollStundenzahl(Row row, Cell cell, Sheet sheet, int rowNumber) {
        row = sheet.getRow(rowNumber);
        cell = row.getCell(6);
        if (cell != null) {
            Double zahl = Double.parseDouble(cell.toString());
            return (zahl.intValue());
        } else {
            return 0;
        }
    }

    public List<Fach> holtListeFaecher(Workbook workbook, int rowNumber) {
        Sheet sheet_0 = workbook.getSheetAt(0);
        List<Fach> liste_Faecher = new ArrayList<>();
        Row row;
        Cell cell;
        HashMap<String, String> listeKuerzelFachname = holtKuerzelFachname(workbook);
        for (int i = 1; i <= 5; i++) {
            row = sheet_0.getRow(rowNumber);
            cell = row.getCell(i);
            if (cell != null) {
                String nameFach = "";
                if (listeKuerzelFachname.containsKey(cell.toString())) {
                    nameFach = listeKuerzelFachname.get(cell.toString());
                }
                Fach fach = new Fach(nameFach, cell.toString(), holtstundenzahlJeKlassenstufe(workbook, cell.toString()));
                liste_Faecher.add(fach);
            }
        }
        return liste_Faecher;
    }

    public List<Fach> holtListeFaecherAlle(Workbook workbook) {
        Sheet sheet_1 = workbook.getSheetAt(1);

        Row row;
        Cell cell;
        String nameFach = "";
        String kuerzel = "";

        HashMap<String, String> listeKuerzelFachname = holtKuerzelFachname(workbook);

        for (int j = 1; j <= 8; j++) {
            row = sheet_1.getRow(j);
            nameFach = row.getCell(0).toString();
            kuerzel = row.getCell(1).toString();
                    if (listeKuerzelFachname.containsKey(kuerzel)) {
                        nameFach = listeKuerzelFachname.get(kuerzel);
                        Fach fach = new Fach(nameFach, kuerzel, holtstundenzahlJeKlassenstufe(workbook, kuerzel));
                        faecher.add(fach);
            }
        }

        return faecher;
    }

    public List<Integer> holtstundenzahlJeKlassenstufe(Workbook workbook, String kuerzelFach) {
        Sheet sheet = workbook.getSheetAt(1);
        int rowNumber;
        List<Integer> listeStundenzahlJeklassenstufe = new ArrayList<Integer>();
        switch (kuerzelFach) {
            case "Ma":
                rowNumber = 1;
                break;
            case "En":
                rowNumber = 2;
                break;
            case "De":
                rowNumber = 3;
                break;
            case "Re":
                rowNumber = 4;
                break;
            case "Ku":
                rowNumber = 5;
                break;
            case "Mu":
                rowNumber = 6;
                break;
            case "Sp":
                rowNumber = 7;
                break;
            case "Su":
                rowNumber = 8;
                break;
            default:
                rowNumber = 0;

        }
        Row row = sheet.getRow(rowNumber);

        for (int i = 2; i <= 5; i++) {
            Cell cell = row.getCell(i);
            if (cell != null) {
                Double zahl = Double.parseDouble(cell.toString());
                listeStundenzahlJeklassenstufe.add(zahl.intValue());
            } else {
                System.out.println("Cell is empty");
            }
        }
        return listeStundenzahlJeklassenstufe;
    }

    public HashMap<String, String> holtKuerzelKlassenname(Workbook workbook) {
        Map listeKuerzelKlassenname = new HashMap<String, String>();
        Sheet sheet = workbook.getSheetAt(2);

        for (int i = 1; i <= 10; i++) {
            Row row = sheet.getRow(i);

            Cell cellKuerzel = row.getCell(0);
            // System.out.println(cellKuerzel);
            Cell cellKlassenname = row.getCell(1);
            // System.out.println(cellKlassenname);
            if (cellKuerzel != null && cellKlassenname != null) {
                listeKuerzelKlassenname.put(cellKuerzel.toString(), cellKlassenname.toString());
                schulklassen.add(new Schulklasse(cellKuerzel.toString(), cellKlassenname.toString()));
            }

        }
        return (HashMap) listeKuerzelKlassenname;
    }

    public HashMap<String, String> holtKuerzelFachname(Workbook workbook) {
        Map listeKuerzelFachname = new HashMap<String, String>();
        Sheet sheet = workbook.getSheetAt(1);

        for (int i = 1; i <= 8; i++) {
            Row row = sheet.getRow(i);

            Cell cellKuerzel = row.getCell(1);
            // System.out.println(cellKuerzel);
            Cell cellFachname = row.getCell(0);
            // System.out.println(cellFachname);
            if (cellKuerzel != null && cellFachname != null) {
                listeKuerzelFachname.put(cellKuerzel.toString(), cellFachname.toString());
            }
        }
        return (HashMap) listeKuerzelFachname;
    }
}
