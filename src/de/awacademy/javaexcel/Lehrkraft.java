package de.awacademy.javaexcel;

import java.util.List;

public class Lehrkraft {

    private String name;

    private List<Fach> faecher;

    private int sollStundenzahl;

    public Lehrkraft(String name, List<Fach> faecher, int sollStundenzahl) {
        this.name = name;
        this.faecher = faecher;
        this.sollStundenzahl = sollStundenzahl;
    }

    public String getName() {
        return name;
    }

    public List<Fach> getFaecher() {
        return faecher;
    }

    public int getSollStundenzahl() {
        return sollStundenzahl;
    }

    @Override
    public String toString() {
        return "Lehrkraft{" +
                "name='" + name + '\'' +
                ", faecher=" + faecher +
                ", sollStundenzahl=" + sollStundenzahl +
                '}';
    }
}
