package de.awacademy.javaexcel;

import java.util.Collection;
import java.util.List;

public class Fach {

    private String name;

    private String abkuerzung;

    private List<Integer> stundenzahlJeKlassenstufe;


    public Fach(String name, String abkuerzung, List<Integer> stundenzahlJeKlassenstufe) {
        this.name = name;
        this.abkuerzung = abkuerzung;
        this.stundenzahlJeKlassenstufe = stundenzahlJeKlassenstufe;
    }

    public String getName() {
        return name;
    }

    public String getAbkuerzung() {
        return abkuerzung;
    }

    public List<Integer> getStundenzahlJeKlassenstufe() {
        return stundenzahlJeKlassenstufe;
    }

    @Override
    public String toString() {
        return "Fach{" +
                "name='" + name + '\'' +
                ", abkuerzung='" + abkuerzung + '\'' +
                ", stundenzahlJeKlassenstufe=" + stundenzahlJeKlassenstufe +
                '}';
    }


}
